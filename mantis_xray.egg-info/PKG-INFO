Metadata-Version: 2.1
Name: mantis-xray
Version: 3.2.2
Summary: MANTiS is a Multivariate ANalysis Tool for x-ray Spectromicroscopy
Home-page: https://spectromicroscopy.com/
Author: Mirna Lerotic
Author-email: mirna@2ndlookconsulting.com
Project-URL: Code, https://github.com/mlerotic/spectromicroscopy
Project-URL: Documentation, https://docs.spectromicroscopy.com
Classifier: Programming Language :: Python :: 3
Classifier: License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
Classifier: Operating System :: OS Independent
Classifier: Topic :: Scientific/Engineering
Requires-Python: >=3
Description-Content-Type: text/markdown
License-File: LICENSE
Requires-Dist: PyQt5>=5.15.9
Requires-Dist: numpy
Requires-Dist: scipy>=1.14.0
Requires-Dist: matplotlib>=3.6.0
Requires-Dist: h5py
Requires-Dist: Pillow
Requires-Dist: lxml
Requires-Dist: pyqtgraph>=0.13.7
Requires-Dist: scikit-image>=0.19.1
Provides-Extra: netcdf
Requires-Dist: netcdf4-python; extra == "netcdf"

# Spectromicroscopy #
[Spectromicroscopy](http://spectromicroscopy.com) combines spectral data with microscopy,
where typical datasets consist of a stack of microscopic images
taken across an energy range. Due to the data complexity, manual analysis 
can be time consuming and inefficient, whereas multivariate analysis tools 
not only reduce the time needed but also can uncover hidden trends in the data.

# Mantis #
[MANTiS](http://spectromicroscopy.com) is Multivariate ANalysis Tool for Spectromicroscopy developed in Python by [2nd Look Consulting](http://2ndlookconsulting.com). It uses principal component analysis and cluster analysis to classify pixels according to spectral similarity.

## Download ##
Mantis package and binaries can be downloaded from 
[spectromicroscopy.com](http://spectromicroscopy.com).
Alternatively, you can install [Python](https://www.python.org/downloads/) and then run the command: `python3 -m pip install mantis-xray`

## Update ##
You can upgrade to the latest package release with the command: `pip3 install mantis-xray -U`.
It is recommended that you also upgrade the dependencies with: `pip3 install mantis-xray -U --upgrade-strategy "eager"`

## Run ##
Installation via pip provides the `mantis-xray` command (alternatively `python3 -m mantis_xray`) to start the Mantis GUI.

## User Guide ##
Mantis User Guide can be found at [https://docs.spectromicroscopy.com/](https://docs.spectromicroscopy.com/).

## References ##

Please use the following reference when quoting Mantis

Lerotic M, Mak R, Wirick S, Meirer F, Jacobsen C. MANTiS: a program for the analysis of X-ray spectromicroscopy data. J. Synchrotron Rad. 2014 Sep; 21(5); 1206–1212 [http://dx.doi.org/10.1107/S1600577514013964]
